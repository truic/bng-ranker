
from scipy.stats import beta

from src.sim_scores import SimilarityScore
from src.sentiment_scores import SentimentScores
from src.readability import ReadabilityMetrics

class Ranking(SimilarityScore, 
              SentimentScores, 
              ReadabilityMetrics):
    
    def __init__(self, data_path, keyword):
        super(Ranking, self).__init__(data_path, keyword)

        self.ppf_score = None
        self.normalized_vector = None

        self.ranks = []

    def __normalize_field_values(self, vector):
        self.normalized_vector = [(float(i)-min(vector))/(max(vector)-min(vector)) for i in vector]

    def __get_percent_point_score(self, a, b):
        self.ppf_score = beta.ppf(0.05, a, b)
        
    def calculate_rank_score(self, data_path, keyword):
        self.get_similarity_scores(data_path, keyword)
        suggested_names = self.sentences.split("\n")
        for num in range(len(suggested_names)):
            a = self.ft_results[num][1] + 1 # self.ft_results gets the fast_text similarity results
            b = self.get_sentiment_score(suggested_names[num]) + 1
            self.__get_percent_point_score(a, b)
            self.run_readability_ranker(suggested_names[num])
            for x in self.readability_scores:
                a = self.ppf_score + 1
                b = self.readability_scores[x] + 1
                self.__get_percent_point_score(a, b)
                if str(self.ppf_score) == "nan":
                    self.ppf_score = 0
            self.ranks.append((suggested_names[num],self.ppf_score))