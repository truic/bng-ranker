
from vaderSentiment.vaderSentiment import SentimentIntensityAnalyzer

from utils.data_prep import DataPrep

class SentimentScores(DataPrep):
    
    def __init__(self):
        super(SentimentScores, self).__init__()
        
        
        self.polarity_score = None
        self.sentiment_scores = None
        
        self.sentiment_scores = []
        
    def __get_vader_sentiment_score(self, text):
        analyzer = SentimentIntensityAnalyzer()
        self.polarity_score = analyzer.polarity_scores(text)
        
    def get_sentiment_score(self, text):
        self.__get_vader_sentiment_score(text)
        return self.polarity_score["compound"]