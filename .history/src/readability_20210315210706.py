
from readability import getmeasures

from utils.data_prep import DataPrep


class ReadabilityMetrics(DataPrep):
    
    def __init__(self, model_path, data_path, keyword):
        super(ReadabilityMetrics, self).__init__(model_path, data_path, keyword)
        
        self.readability_result = None
        
        self.kincaid = None
        self.ari = None
        self.coleman_liau = None
        self.flesch_reading_ease = None
        self.gunning_fog_index = None
        self.lix = None
        self.smog_index = None
        self.rix = None
        self.dale_chall_index = None

        self.readability_scores = {}
        
    def __get_readability_results(self, text):
        self.readability_result = getmeasures(text, lang='en')
        
    def __get_kincaid_score(self):
        self.kincaid = self.readability_result['readability grades']['Kincaid']
        self.readability_scores["Kincaid"] = self.kincaid
        
    def __get_ari_score(self):
        self.ari = self.readability_result['readability grades']['ARI']
        self.readability_scores["ARI"] = self.ari
        
    def __get_coleman_liau_score(self):
        self.coleman_liau = self.readability_result['readability grades']['Coleman-Liau']
        self.readability_scores["Coleman-Liau"] = self.coleman_liau
        
    def __get_flesch_reading_ease_score(self):
        self.flesch_reading_ease = self.readability_result['readability grades']['FleschReadingEase']
        self.readability_scores["FleschReadingEase"] = self.flesch_reading_ease
        
    def __get_gunning_fog_index_score(self):
        self.gunning_fog_index = self.readability_result['readability grades']['GunningFogIndex']
        self.readability_scores["GunningFogIndex"] = self.gunning_fog_index
        
    def __get_lix_score(self):
        self.lix = self.readability_result['readability grades']['LIX']
        self.readability_scores["LIX"] = self.lix
        
    def __get_smog_index_score(self):
        self.smog_index = self.readability_result['readability grades']['SMOGIndex']
        self.readability_scores["SMOGIndex"] = self.smog_index
        
    def __get_rix_score(self):
        self.rix = self.readability_result["readability grades"]['RIX']
        self.readability_scores["RIX"] = self.rix
        
    def __get_dale_chall_index_score(self):
        self.dale_chall_index = self.readability_result["readability grades"]["DaleChallIndex"]
        self.readability_scores["DaleChallIndex"] = self.dale_chall_index

    def run_readability_ranker(self, text):
        self.__get_readability_results(text)
        self.__get_kincaid_score()
        self.__get_ari_score()
        self.__get_coleman_liau_score()
        self.__get_flesch_reading_ease_score()
        self.__get_gunning_fog_index_score()
        self.__get_lix_score()
        self.__get_smog_index_score()
        self.__get_rix_score()
        self.__get_dale_chall_index_score()
