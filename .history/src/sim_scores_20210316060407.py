
from gensim.models import FastText

from fse.models import Average
from fse import IndexedList

from utils.data_prep import DataPrep

class SimilarityScore(DataPrep):
    
    def __init__(self):
        super(SimilarityScore, self).__init__()    
        
        self.ft_embeddings = None
        self.ft_model = None
        self.ft_results = None
        
        self.glove_embeddings = None
        self.glove_model = None
        self.glove_results = None
        
        self.embedding = "FastText"
        
    def __get_ft_embeddings(self):
        self.ft_embeddings = FastText(self.split_sents)
        
    def __train_ft_model(self):
        self.ft_model = Average(self.ft_embeddings)
        self.ft_model.train(IndexedList(self.split_sents))
        
    def __get_ft_similar_sents(self):
        self.ft_results = self.ft_model.sv.most_similar(0, topn=len(self.split_sents))

    def __get_glove_embeddings(self):
        pass
    
    def __train_glove_model(self):
        pass
    
    def __get_glove_similar_sents(self):
        pass
    
    def get_similarity_scores(self):
        if self.embedding == "FastText":
            self.split_sentences(data_path, key)
            self.__get_ft_embeddings()
            self.__train_ft_model()
            self.__get_ft_similar_sents()
        elif self.embedding == "Glove":
            self.__get_glove_embeddings()
            self.__train_glove_model()
            self.__get_glove_similar_sents()