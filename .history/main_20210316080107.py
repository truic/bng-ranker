
import json
import configparser
# import argparse

from flask import Flask, Response, request
from flask_classful import FlaskView
from flask_classful import route

from ranker import Ranking

config = configparser.ConfigParser()
config.read('./config/dev.ini')

app = Flask(__name__)

@app.route('/upload', methods=['POST'])
def upload_file():
   if request.method == 'POST':
       dict = request.get_json(force=True)
       global file_path
       file_path = dict["data_path"]
       global key_word = dict["keyword"]
       return 'file uploaded successfully'

data_path = config.get("BNG", "data_path")
keyword = config.get("BNG", "keyword")

# parser = argparse.ArgumentParser(description='BNG Ranker')
# parser.add_argument('upload', type=str, help='Command to upload a file or not')
# parser.add_argument('keyword', type=str, help='Input keyword for the BNG ranker')
# parser.add_argument('data', type=str, help='Input file path for the names')

# args_namespace, unknown = parser.parse_known_args()
# args = vars(args_namespace)

class MultiDimensionalArrayEncoder(json.JSONEncoder):
    def encode(self, obj):
        def hint_tuples(item):
            if isinstance(item, tuple):
                return {'__tuple__': True, 'items': item}
            if isinstance(item, list):
                return [hint_tuples(e) for e in item]
            if isinstance(item, dict):
                return {key: hint_tuples(value) for key, value in item.items()}
            else:
                return item

        return super(MultiDimensionalArrayEncoder, self).encode(hint_tuples(obj))


class BusinessNameRanker(FlaskView,
                            Ranking):
    
    def __init__(self):
        # if args["upload"] == "n":
        #     super().__init__(data_path, keyword)
        # elif args["upload"] == "y":
        #     super().__init__(args["data"], args["keyword"])
        
        super().__init__(data_path, keyword)    
        
        self.enc = MultiDimensionalArrayEncoder()
        
        self.s2v_names = None
        self.rank_score = []     
        
    @route('/get_ranked_results/')    
    def get_ranked_results(self):
        self.data_path = file_path
        self.keyword = key_word
        self.calculate_rank_score()
        self.rank_score = self.ranks
        jsonstring = self.enc.encode({"Ranked Results": self.rank_score})
        resp = Response(jsonstring, status=200, mimetype='application/json')
        return resp
    
    @route('/sort_ranked_results/')
    def sort_business_name_ranker(self):
        self.data_path = file_path
        self.keyword = key_word
        self.update()
        self.calculate_rank_score()
        print(sorted(self.ranks, key = lambda x: x[1]))
        jsonstring = self.enc.encode({"Sorted Results": sorted(self.ranks, key = lambda x: x[1])})
        resp = Response(jsonstring, status=200, mimetype='application/json')
        return resp


BusinessNameRanker.register(app, route_base='/bngranker')

if __name__ == '__main__':
    app.run(host="0.0.0.0", port="5100")