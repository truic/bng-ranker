
import json
import configparser

from flask import Flask, Response
from flask_classful import FlaskView
from flask_classful import route

from ranker import Ranking


class BusinessNameGenerator(Ranking):
    
    def __init__(self,                      
                 model_path,
                 data_path,
                 keyword):
        super(BusinessNameGenerator, self).__init__(model_path, data_path, keyword)
        
        self.s2v_names = None
        self.rank_score = []
        
    def __get_bng_api(self):
        pass
        
    def __get_gpt2_names(self):
        pass
    
    def get_sense2vec_names(self):
        self.get_most_similar_names()
        self.s2v_names = self.sense_results
        # self.write_data(self.s2v_names)
        
    def get_ranked_results(self):
        self.calculate_rank_score()
        self.rank_score = self.ranks
    
    def __get_business_name_ranker(self):
        self.ranked_names = self.ranked_results

        

model_path = "./sense2vector/models/s2v_reddit_2019_lg/"
data_path = "./data/names.txt"
keyword = "beach_restaurant"
bng = BusinessNameGenerator(model_path, data_path, keyword)
# bng.load_sense_models()
# bng.get_sense2vec_names()
bng.get_ranked_results()
print(bng.rank_score)