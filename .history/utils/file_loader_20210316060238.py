

class FileObject:
    
    def __init__(self):
        
        super(FileObject, self).__init__()
        
        self.sentences = None
        
    def load_data(self, data_path):
        with open(data_path) as f:
            self.sentences = f.read()
            
    def load_model(self):
        pass
    
    def write_data(self, data_path, data):
        with open(self.data_path) as f:
            f.write("\n".join(data))