
from .file_loader import FileObject

config = configparser.ConfigParser()
config.read('./config/dev.ini')

upload = config.get("API CONFIG", "upload")

class DataPrep(FileObject):
    
    def __init__(self, data_path, keyword):
        if upload == "no":
            super(DataPrep, self).__init__(data_path)
        elif upload == "yes":
            super(DataPrep, self).__init__()    
        
        
        self.split_sents = None
        
    def split_sentences(self):
        self.load_data()
        self.split_sents = [self.keyword.split(" ")] + \
            [x.lower().split(" ") for x in self.sentences.split("\n")]    