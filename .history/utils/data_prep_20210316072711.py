
from .file_loader import FileObject

class DataPrep(FileObject):
    
    def __init__(self, data_path, keyword):
        super(DataPrep, self).__init__(DAT)    
        
        self.split_sents = None
        
    def split_sentences(self):
        self.load_data()
        self.split_sents = [self.keyword.split(" ")] + \
            [x.lower().split(" ") for x in self.sentences.split("\n")]    