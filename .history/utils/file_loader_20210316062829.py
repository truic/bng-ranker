

class FileObject:
    
    def __init__(self, data_path, keyword):
        
        super(FileObject, self).__init__()
        
        self.sentences = None
        
    def load_data(self, data_path):
        with open(data_path) as f:
            self.sentences = f.read()
            
    def load_model(self):
        pass