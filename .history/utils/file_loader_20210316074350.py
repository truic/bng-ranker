

class FileObject:
    
    def __init__(self, data_path):
        
        super(FileObject, self).__init__()
        

        self.data_path = data_path
        
        self.sentences = None
        
    def load_data(self):
        with open(self.data_path) as f:
            self.sentences = f.read()
            
    def load_model(self):
        pass
    
    def write_data(self, data):
        with open(self.data_path) as f:
            f.write("\n".join(data))