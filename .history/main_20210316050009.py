
import json
import configparser

from flask import Flask, Response
from flask_classful import FlaskView
from flask_classful import route

from ranker import Ranking

config = configparser.ConfigParser()
config.read('./config/dev.ini')

app = Flask(__name__)

data_path = config.get("BNG", "data_path")
keyword = config.get("BNG", "keyword")

class MultiDimensionalArrayEncoder(json.JSONEncoder):
    def encode(self, obj):
        def hint_tuples(item):
            if isinstance(item, tuple):
                return {'__tuple__': True, 'items': item}
            if isinstance(item, list):
                return [hint_tuples(e) for e in item]
            if isinstance(item, dict):
                return {key: hint_tuples(value) for key, value in item.items()}
            else:
                return item

        return super(MultiDimensionalArrayEncoder, self).encode(hint_tuples(obj))


class BusinessNameRanker(FlaskView,
                            Ranking):
    
    def __init__(self):
        super().__init__(data_path, keyword)
        
        self.enc = MultiDimensionalArrayEncoder()
        
        self.s2v_names = None
        self.rank_score = []
        
    @route('/get_ranked_results/')    
    def get_ranked_results(self):
        self.calculate_rank_score()
        self.rank_score = self.ranks
        jsonstring = self.enc.encode({"names":self.sentences, "Rank Scores": self.rank_score})
        resp = Response(jsonstring, status=200, mimetype='application/json')
        return resp
    
    @route('/sort_ranked_results/')
    def __get_business_name_ranker(self):
        self.ranked_names = self.ranked_results

        


bng = BusinessNameGenerator(model_path, data_path, keyword)
# bng.load_sense_models()
# bng.get_sense2vec_names()
bng.get_ranked_results()
print(bng.rank_score)